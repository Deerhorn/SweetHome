import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import { Task } from 'c:/Users/Deerhorn/sweet/src/pages/sweet/task';


@Injectable()
export class HttpDataProvider {

private readonly baseUrl: string = 'https://api2.bmob.cn/1';
private readonly httpOptions: Object = {
  headers : new HttpHeaders({
   'X-Bmob-Application-Id': 'fd1b2d7e99e41de61fa6104d090b7d97',
   'X-Bmob-REST-API-Key': '39e091639ef21609594c48c03a5a8d01',
  'Content-Type': 'application/json',
  }

  )
};

  constructor(public http: HttpClient) {
    console.log('获取HTTP数据服务加载');
  }


  getTaskList(): Observable<Task[]>{
    return this.http.get<Task[]>(`${this.baseUrl}/classes/ToDolist`, this.httpOptions).map(data => data["results"]);

  }
  addTask(task: Task): Observable<Task>{
    let _task = {title: task.title, status: task.status, description :task.description};
    return this.http.post<Task>(`${this.baseUrl}/classes/ToDolist/`, _task, this.httpOptions);
  }
/**
 * 
 * @param task 
 * @returns {Observable<Task[]>} 响应结果
 */
  updateTask(task : Task): Observable<Task>{
    let _task = {title: task.title, status: task.status, description :task.description};
    return this.http.put<Task>(`${this.baseUrl}/classes/ToDolist/${task.objectId}`, _task, this.httpOptions);

  }


  deleteTask(task : Task):Observable<{}> {
    return this.http.delete<Task>(`${this.baseUrl}/classes/ToDolist/${task.objectId}`,  this.httpOptions);
  }

}
