import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { SplashScreen } from '@ionic-native/splash-screen';
import { StatusBar } from '@ionic-native/status-bar';

import { MyApp } from './app.component';
import { sweetPage } from '../pages/sweet/sweet';
import { HttpDataProvider } from 'c:/Users/Deerhorn/sweet/src/pages/providers/http-data/http-data';
import { HttpClientModule } from '@angular/common/http';
import { InputTaskPage } from '../pages/input-task/input-task';

@NgModule({
  declarations: [
    MyApp,
    sweetPage
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    IonicModule.forRoot(MyApp)
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    sweetPage
  ],
  providers: [
    StatusBar,
    SplashScreen,
    {provide: ErrorHandler, useClass: IonicErrorHandler}
  ]
})
export class AppModule {}
