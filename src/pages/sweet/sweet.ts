import { Component } from '@angular/core';
import { NavController, ItemSliding , ModalController } from 'ionic-angular';
import { Task } from'c:/Users/Deerhorn/sweet/src/pages/sweet/task';
import { InputTaskPage } from '../input-task/input-task';
import { HttpDataProvider } from'c:/Users/Deerhorn/ToDo/src/providers/http-data/http-data'
@Component({
  selector: 'page-sweet',
  templateUrl: 'sweet.html'
})
export class sweetPage {

  tasks: Task[]= [];

  constructor(public navCtrl: NavController,
      public modalCtrl: ModalController,
     public httpData: HttpDataProvider
    ) {

  }
 ionViewDidLoad(){
 //this.tasks = [
 //{ title: '牛奶',status:'open'},
 //{ title: '鸡蛋',status:'open'},
 //{ title: '果汁',status:'open'},
 //{ title: '煎饼',status:'open'}

// ];
this.httpData.getTaskList().subscribe(tasks =>{ 
  this.tasks = tasks;
  console.log(tasks);
})
}

  addTask(){
//let theNewTask: string = prompt("新任务");
//if(theNewTask !==''){
  //this.tasks.push({title: theNewTask, status: 'open'});
//}
let addModal = this.modalCtrl.create(InputTaskPage);

addModal.onDidDismiss((task: Task) =>{
if(task){
  this.tasks.push(task);
}
});

addModal.present();
  }

  editTask(task: Task){
if(task.status === 'done')
return;
const index : number = this.tasks.indexOf(task);
let editModal = this.modalCtrl.create(InputTaskPage, {task: task});

editModal.onDidDismiss((task )=>{
if(task){
this.tasks[index] = task;
}
});
editModal.present();
  }

  markAsDone(slidingItem: ItemSliding, task: Task){

    task.status ='done';
    this.httpData.updateTask(task).subscribe();
    slidingItem.close();
  }

  removeTask(slidingItem: ItemSliding, task: Task){
task.status ='removed';
let index =this.tasks.indexOf(task);
if(index > -1){
  this.tasks.splice(index, 1);
}
this.httpData.deleteTask(task).subscribe();
slidingItem.close();
  }
  doRefresh(refresher: any){
    setTimeout(() => {
      this.httpData.getTaskList().subscribe(tasks =>{ 
        this.tasks = tasks;
      });
      refresher.complete();
    }, 2000);
  }

}
